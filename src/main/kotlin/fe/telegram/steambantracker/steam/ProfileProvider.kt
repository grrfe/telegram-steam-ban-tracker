package fe.telegram.steambantracker.steam

import fe.telegram.steambantracker.REQUEST
import fe.telegram.steambantracker.api.ExistsProfileRequest
import fe.telegram.steambantracker.api.FindIdRequest
import java.util.regex.Matcher
import java.util.regex.Pattern

object ProfileProvider : SteamUrlIdProvider(Pattern.compile("https?:\\/\\/steamcommunity\\.com\\/profiles\\/(\\d+)\\/?")) {
    override fun provideId(matcher: Matcher) = matcher.group(1).toLong().let { id ->
        if (REQUEST.send(ExistsProfileRequest(id))) {
            id
        } else null
    }
}

object IdProvider : SteamUrlIdProvider(Pattern.compile("https?:\\/\\/steamcommunity\\.com\\/id\\/(.+)\\/?")) {
    override fun provideId(matcher: Matcher) = REQUEST.send(FindIdRequest(matcher.group(1)))
}

abstract class SteamUrlIdProvider(private val pattern: Pattern) {
    companion object {
        private val PROFILE_PROVIDER = listOf(IdProvider, ProfileProvider)

        fun findId(url: String): Long? {
            PROFILE_PROVIDER.forEach { provider ->
                provider.tryFetchId(url)?.let {
                    return it
                }
            }

            return null
        }
    }

    fun tryFetchId(url: String): Long? {
        val matcher = pattern.matcher(url)
        return if (matcher.matches()) {
            this.provideId(matcher)
        } else null
    }

    abstract fun provideId(matcher: Matcher): Long?
}
