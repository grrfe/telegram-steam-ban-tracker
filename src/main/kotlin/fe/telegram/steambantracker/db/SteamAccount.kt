package fe.telegram.steambantracker.db

import org.jetbrains.exposed.dao.Entity
import org.jetbrains.exposed.dao.EntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IdTable

object SteamAccounts : IdTable<Long>("steam_account") {
    override val id = long("steam_id").entityId()
    val chatId = long("chatId")

    override val primaryKey = PrimaryKey(id, chatId)
}

class SteamAccount(id: EntityID<Long>) : Entity<Long>(id) {
    companion object : EntityClass<Long, SteamAccount>(SteamAccounts)

    var steamId by SteamAccounts.id
    var chatId by SteamAccounts.chatId
}

