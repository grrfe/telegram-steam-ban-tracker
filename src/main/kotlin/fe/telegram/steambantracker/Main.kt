package fe.telegram.steambantracker

import com.google.gson.Gson
import com.pengrad.telegrambot.TelegramBot
import com.pengrad.telegrambot.UpdatesListener
import com.pengrad.telegrambot.model.Update
import com.pengrad.telegrambot.request.SendMessage
import fe.logger.Logger
import fe.request.Request
import fe.request.proxy.AuthProxy
import fe.request.proxy.PasswordProxyLogin
import fe.telegram.steambantracker.api.BanRequest
import fe.telegram.steambantracker.cmd.Command
import fe.telegram.steambantracker.config.Config
import fe.telegram.steambantracker.db.SteamAccount
import fe.telegram.steambantracker.db.SteamAccounts
import okhttp3.OkHttpClient
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.deleteWhere
import org.jetbrains.exposed.sql.transactions.transaction
import java.io.BufferedReader
import java.io.FileReader
import java.net.Authenticator
import java.net.InetSocketAddress
import java.net.Proxy
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit


class UpdateListener(private val bot: TelegramBot) : UpdatesListener {
    override fun process(updates: MutableList<Update>): Int {
        updates.forEach { update ->
            if (!Command.execMessageCmd(update.message(), bot)) {
                bot.execute(SendMessage(update.message().chat().id(), "Invalid command!\nAvailable commands: ${Command.CMD_LIST}"))
            }
        }

        return UpdatesListener.CONFIRMED_UPDATES_ALL
    }
}

class BanStatusChecker(private val bot: TelegramBot) : Runnable {
    private val STEAM_URL = "https://steamcommunity.com/profiles/%s"

    override fun run() {
        LOGGER.print(Logger.Type.INFO, "Checking all tracked accounts now..")
        val ownerMap = transaction { SteamAccount.all().associateBy({ it.steamId.value }, { it.chatId }) }
        REQUEST.send(BanRequest(ownerMap.map { it.key.toString() })).filter { it.hasBan() }.forEach { status ->
            val chatId = ownerMap[status.id]
            bot.execute(
                SendMessage(
                    chatId,
                    "Ouch! Looks like your account ${STEAM_URL.format(status.id)} got ${status.getBanType()}! Untracking now."
                )
            )

            transaction {
                SteamAccounts.deleteWhere {
                    SteamAccounts.id eq status.id
                }
            }
        }
    }
}


val GSON = Gson()
lateinit var CONFIG: Config
var REQUEST = Request()

val LOGGER = Logger("SteamBanTracker")

fun main(args: Array<String>) {
    if (args.size != 1) {
        error("Config required!")
    }

    CONFIG = BufferedReader(FileReader(args[0])).use {
        GSON.fromJson(it, Config::class.java)
    }
    LOGGER.print(Logger.Type.INFO, "Started bot with config $CONFIG")

    val httpClient = CONFIG.proxy?.let { proxyConfig ->
        val proxy = Proxy(proxyConfig.type, InetSocketAddress(proxyConfig.host, proxyConfig.port))
        val login = if (proxyConfig.username != null && proxyConfig.password != null) {
            PasswordProxyLogin(proxyConfig.username, proxyConfig.password).also {
                Authenticator.setDefault(it.authenticator)
            }
        } else null

        REQUEST.data.proxy = AuthProxy(proxy, login)
        OkHttpClient.Builder().proxy(proxy).build()
    } ?: OkHttpClient()

    Database.connect("jdbc:sqlite:database.sqlite")
    transaction {
        SchemaUtils.createMissingTablesAndColumns(SteamAccounts)
    }

    val bot = TelegramBot.Builder(CONFIG.botToken)
        .okHttpClient(httpClient)
        .build()
    bot.setUpdatesListener(UpdateListener(bot))

    val scheduler = Executors.newScheduledThreadPool(1)
    scheduler.scheduleAtFixedRate(BanStatusChecker(bot), CONFIG.initDelay, CONFIG.period, TimeUnit.MINUTES)
}