package fe.telegram.steambantracker.config

import java.net.Proxy

data class Config(
    val botToken: String,
    val initDelay: Long,
    val period: Long,
    val proxy: ProxyConfig?
)

data class ProxyConfig(
    val host: String,
    val port: Int,
    val type: Proxy.Type,
    val username: String?,
    val password: String?
)