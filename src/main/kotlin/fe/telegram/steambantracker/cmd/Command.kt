package fe.telegram.steambantracker.cmd

import com.pengrad.telegrambot.TelegramBot
import com.pengrad.telegrambot.model.Message
import com.pengrad.telegrambot.model.User
import com.pengrad.telegrambot.request.SendMessage

abstract class Command(val name: String, val usage: String, val description: String) {
    companion object {
        private val COMMANDS = listOf(TrackAccountCommand)

        val CMD_LIST by lazy {
            COMMANDS.joinToString(separator = "\n") { "${it.name} ${it.usage}: ${it.description}" }
        }

        fun execMessageCmd(msg: Message, bot: TelegramBot): Boolean {
            COMMANDS.forEach { cmd ->
                msg.text()?.let { msgText ->
                    if (msgText.startsWith(cmd.name)) {
                        cmd.exec(
                            msgText.split(" ").run {
                                this.subList(1, this.size)
                            },
                            bot,
                            msg.from(),
                            msg.chat().id()
                        )

                        return true
                    }
                }
            }

            return false
        }
    }

    abstract fun exec(args: List<String>, bot: TelegramBot, user: User, chat: Long)

    fun sendUsage(chat: Long) = SendMessage(chat, "Usage: $name $usage")
}