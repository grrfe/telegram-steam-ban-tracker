package fe.telegram.steambantracker.cmd

import com.pengrad.telegrambot.TelegramBot
import com.pengrad.telegrambot.model.User
import com.pengrad.telegrambot.request.SendMessage
import fe.telegram.steambantracker.db.SteamAccount
import fe.telegram.steambantracker.db.SteamAccounts
import fe.telegram.steambantracker.steam.SteamUrlIdProvider
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.sql.transactions.transaction

object TrackAccountCommand : Command("/track", "<profile_url>", "Add a steam account for the bot to track") {
    override fun exec(args: List<String>, bot: TelegramBot, user: User, chat: Long) {
        if (args.size == 1) {
            val id = SteamUrlIdProvider.findId(args[0])
            if (id != null) {
                try {
                    transaction {
                        SteamAccount.new {
                            this.steamId = EntityID(id, SteamAccounts)
                            this.chatId = chat
                        }
                    }

                    bot.execute(SendMessage(chat, "Success! Account has been added and will now be tracked."))
                } catch (e: Exception) {
                    println(e.message)
                }

            } else {
                bot.execute(SendMessage(chat, "This account does not seem to exist!"))
            }
        } else {
            bot.execute(this.sendUsage(chat))
        }
    }
}