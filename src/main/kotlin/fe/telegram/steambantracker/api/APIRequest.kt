package fe.telegram.steambantracker.api

import com.google.gson.JsonArray
import com.google.gson.JsonObject
import fe.request.clazz.GetRequest
import fe.request.util.readToJson
import fe.request.util.readToString
import java.net.HttpURLConnection

//found this key on github idk
const val KEY = "FBE6A5C249453A8516A55AAD5F87973F"

data class BanStatus(val id: Long, val communityBan: Boolean, val vacBan: Boolean, val gameBan: Boolean) {
    fun hasBan() = communityBan || vacBan || gameBan

    fun getBanType(): String {
        return (when {
            communityBan -> {
                "community"
            }
            vacBan -> {
                "VAC"
            }
            else -> {
                "game"
            }
        }) + " banned"
    }
}

class BanRequest(ids: List<String>) :
    GetRequest<List<BanStatus>>(
        "http://api.steampowered.com/ISteamUser/GetPlayerBans/v1/?key=$KEY&steamids=${
            ids.joinToString(
                separator = ","
            )
        }}}&format=json"
    ) {
    override fun handleResult(con: HttpURLConnection): List<BanStatus> {
        val resp = con.readToJson() as JsonObject
        val players = resp.get("players") as JsonArray

        return players.map { elem ->
            with(elem as JsonObject) {
                BanStatus(
                    this.get("SteamId").asString.toLong(),
                    this.get("CommunityBanned").asBoolean,
                    this.get("VACBanned").asBoolean,
                    this.get("NumberOfGameBans").asInt > 0
                )
            }
        }
    }
}

class FindIdRequest(url: String) :
    GetRequest<Long?>("http://api.steampowered.com/ISteamUser/ResolveVanityURL/v0001/?key=$KEY&vanityurl=$url&format=json") {
    override fun handleResult(con: HttpURLConnection): Long? {
        val obj = con.readToJson() as JsonObject
        with(obj.get("response") as JsonObject) {
            if (this.get("success").asInt == 1) {
                return this.get("steamid").asString.toLong()
            }
        }

        return null
    }
}

class ExistsProfileRequest(id: Long) : GetRequest<Boolean>("https://steamcommunity.com/profiles/$id") {
    override fun handleResult(con: HttpURLConnection): Boolean {
        return !con.readToString().contains("The specified profile could not be found.")
    }
}