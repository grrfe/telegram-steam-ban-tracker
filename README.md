# telegram-steam-ban-tracker

Track the ban-status of your steam accounts via Telegram.

<div align="center">
  <img src="screen/screenshot.png" alt="">
</div>

## Requirements

* Java 8+
* Telegram account  
* Basic JSON Knowledge (optional)

## Downloads

[Releases](https://gitlab.com/grrfe/telegram-steam-ban-tracker/-/releases)

## How to use

Copy the [example_config.json](doc/example_config.json) to the same directory as the release jar. Edit the `.json` file according to your needs. 


If you don't want to use a proxy, you can remove the whole `"proxy": {...}` object. If you don't want proxy authentication, 
remove `"username": ""` and `"password": ""`. Available proxy `type`s: `SOCKS` and `HTTP`.


To obtain a `botToken`, follow [this tutorial](https://core.telegram.org/bots#3-how-do-i-create-a-bot).
You may also want to register the `/track` command with the bot like this: In your chat with `@BotFather`, 
select your bot, then click `Edit Bot`, then `Edit Commands` and send this line: `track - Add a steam account for the bot to track`

`initDelay` and `period` are in minutes and may only be integers. `initDelay` specifies how long the bot waits 
before checking all tracked accounts for bans after it has first been started, while `period` specifies how long it 
will wait before checking again.

The bot will create a `database.sqlite` file where the tracked steamid64's and the owner chat ids are stored.

Launch the jar with `java -jar telegram-steam-ban-tracker-%VERSION%.jar %yourconfig%.json`